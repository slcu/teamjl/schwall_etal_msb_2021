This file contains all of the data from the simulations where an additional operon (containing a subset of the system components) is added to the system (the additional operon is still activated by SigV).

(I also needed to create some file in this folder to ensure it got uploaded by got, and figured this would be better than a .gitignore file)
