This file contains all of the data from when the baseline expression is increased of either component (practically this can be achived by adding an additional operon which is induced, but then not induce it. That additional operon will the leack a little). This case is very similar to the "increased_baseline_expression" case, but that one also have some figures summing up the result.

(I also needed to create some file in this folder to ensure it got uploaded by got, and figured this would be better than a .gitignore file)
