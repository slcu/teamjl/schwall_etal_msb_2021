# Special Function which saves a simulation set ot csv
function resave_as_cvs(folder,filename;vars=[1])
    sols = deserialize(folder*filename*".jls")
    idxs = filter(i->sols[1].t[i] != sols[1].t[i+1],1:(length(sols[1].t)-1))
    for (idx,species) in enumerate(["SigV","RsiV","SigVRsiV"])
        !in(idx,vars) && continue
        file = open(folder*filename*"_"*species*".csv", "w")
        foreach(i -> write(file, "$(sols[1].t[i]),"), idxs);  write(file, "$(sols[1].t[end])\n");
        for sol in sols
            foreach(i -> write(file, "$(sol.u[i][idx]),"), idxs);  write(file, "$(sol.u[end][idx])\n");            
        end        
        close(file)
    end
end;