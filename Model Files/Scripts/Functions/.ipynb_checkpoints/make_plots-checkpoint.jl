# Plots a simulation.
plot_sims(args...;kwargs...) = (plot(); plot_sims!(args...;kwargs...);)
function plot_sims!(sols;var=1,linewidth=1.,linealpha=0.5,startidx=1,framestyle=:box,grid=false,xguide="Time (Minutes)",yguide="σⱽ molecules",guidefontsize=13,tickfontsize=9,right_margin=3mm,title="",titlefontsize=20,ylimit=nothing,color=nothing,kwargs...)
    (color==nothing) && foreach(sim -> plot!(sim.t[startidx:end],getindex.(sim.u,var)[startidx:end],linewidth=linewidth,linealpha=linealpha,label=""), sols)
    (color!=nothing) && foreach(sim -> plot!(sim.t[startidx:end],getindex.(sim.u,var)[startidx:end],linewidth=linewidth,linealpha=linealpha,color=color,label=""), sols)
    plot!(framestyle=framestyle,grid=grid,xguide=xguide,yguide=yguide,guidefontsize=guidefontsize,tickfontsize=tickfontsize,xlimit=(sols[1].t[1],sols[1].t[end]),right_margin=right_margin,title=title,titlefontsize=titlefontsize,ylimit=ylimit)
end

# Plots a pattern of stress activation/deactivation.
function plot_stress_pattern(addition_times,removal_times,heigth;lw=3.5,ls=:dot,c1=:red,c2=:black)
    plot()
    foreach(at -> plot!([at,at],[0,heigth],label="",linewidth=lw,linestyle = ls,color=c1), addition_times)
    foreach(rt -> plot!([rt,rt],[0,heigth],label="",linewidth=lw,linestyle = ls,color=c2), removal_times)
    return plot!()
end;
function plot_stress_pattern!(addition_times,removal_times,heigth;lw=3.5,ls=:dot,c1=:red,c2=:black)
    foreach(at -> plot!([at,at],[0,heigth],label="",linewidth=lw,linestyle = ls,color=c1), addition_times)
    foreach(rt -> plot!([rt,rt],[0,heigth],label="",linewidth=lw,linestyle = ls,color=c2), removal_times)
    return plot!()
end;

# Plots a pattern of stress activation/deactivation and induction.
function plot_stress_induction_pattern(addition_times,removal_times,induction_times,heigth)
    plot_stress_pattern(addition_times,removal_times,heigth)
    foreach(it -> plot!([it,it],[0,heigth],label="",linewidth=3.5,linestyle = :dot,color=:purple), induction_times)
    return plot!()
end;
function plot_stress_induction_pattern!(addition_times,removal_times,induction_times,heigth)
    plot_stress_pattern!(addition_times,removal_times,heigth)
    foreach(it -> plot!([it,it],[0,heigth],label="",linewidth=3.5,linestyle = :dot,color=:purple), induction_times)
    return plot!()
end;

# Plots the cummulative activation plots for a single activation simulation.
function cummulative_activation_plot(sols,min_thres,max_thres;start=500,var=1,kwargs...)
    distribution = map(i ->  mean(map(sol -> on_value(sol.u[i][var],min_thres,max_thres), sols)), 1:length(sols[1].t))
    return plot(sols[1].t, distribution; xlabel="Time (Minutes)", ylabel = "Fraction of activated cells",legend=:bottomright,framestyle=:box,grid=false,guidefontsize=13,labelfontsize=13,tickfontsize=9,right_margin=3mm,title="",titlefontsize=20,kwargs...)
end
function cummulative_activation_plot!(sols,min_thres,max_thres;start=500,var=1,kwargs...)
    distribution = map(i ->  mean(map(sol -> on_value(sol.u[i][var],min_thres,max_thres), sols)), 1:length(sols[1].t))
    return plot!(sols[1].t, distribution; xlabel="Time (Minutes)", ylabel = "Fraction of activated cells",legend=:bottomright,framestyle=:box,grid=false,guidefontsize=13,labelfontsize=13,tickfontsize=9,right_margin=3mm,title="",titlefontsize=20,kwargs...)
end

# Plots the cummulative activation plots for a set of activation simulations.
function cummulative_activation_plot!(solution_sets,variation_values,selected_values,label_base,min_thres,max_thres;start=500,var=1,kwargs...)
    distributions = Matrix{Float64}(undef,length(solution_sets[1][1].t),length(selected_values))
    for i = 1:length(selected_values), L = 1:length(solution_sets[1][1].t)
        activity_values = map(M -> on_value(solution_sets[selected_values[i]][M].u[L][var],min_thres,max_thres), 1:length(solution_sets[selected_values[i]]))
        distributions[L,i] = mean(activity_values)
    end
    labels = Array{String,2}(undef,1,length(selected_values))
    foreach(i -> labels[i] = label_base*":   "*string(variation_values[selected_values[i]]) , 1:length(selected_values))
    return plot!(solution_sets[1][1].t, distributions; label = labels, xlabel="Time (Minutes)", ylabel = "Fraction of activated cells",legend=:bottomright,framestyle=:box,grid=false,guidefontsize=13,labelfontsize=13,tickfontsize=9,right_margin=3mm,title="",titlefontsize=20,kwargs...)
end

# Calculates the degree with which a simulation is "on".
on_value(val,minV,maxV) = max(0,min(1,(val-minV)/(maxV-minV)));