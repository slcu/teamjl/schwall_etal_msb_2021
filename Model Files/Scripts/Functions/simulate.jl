### For base simulations ###
function ssa_monte(p_vals,tspan,n,stress_steps;jumps=jumps_base,solver=SSAStepper(),eSolver=EnsembleThreads(),saveat=1.,find_u0=ssa_get_u0_base)
    u0s_cur = fill(find_u0(p_vals,jumps),n)
    tstarts = vcat([tspan[1]],map(stress_step -> stress_step[1],stress_steps)...)
    tends = vcat(map(stress_step -> stress_step[1],stress_steps)...,[tspan[2]])
    stress_levels = vcat([p_vals[9]],map(stress_step -> stress_step[2],stress_steps)...)
    sols = Vector{Any}(undef,length(tstarts))
    for i = 1:(length(stress_steps)+1)
        disc_prob = DiscreteProblem(u0s_cur, (tstarts[i],tends[i]), setindex!(copy(p_vals),stress_levels[i],9))
        jump_prob = JumpProblem(disc_prob, Direct(),jumps...,save_positions=(false,false))
        ensemble_prob = EnsembleProblem(jump_prob,prob_func=(p,i,r)->JumpProblem(remake(p.prob,u0=u0s_cur[i]), Direct(),save_positions=(false,false), jumps...))
        sols[i] = solve(ensemble_prob,solver,eSolver,trajectories=n,saveat=saveat) 
        u0s_cur = map(sol -> sol.u[end], sols[i])
    end
    return map(i -> (t=vcat(map(sol -> sol[i].t,sols)...), u=vcat(map(sol -> sol[i].u,sols)...)), 1:n)
end
function ssa_get_u0_base(p_vals,jumps)
    disc_prob = DiscreteProblem([2,2,2], (0.,100.), p_vals)
    jump_prob = JumpProblem(disc_prob, Direct(), jumps...)
    return solve(jump_prob, SSAStepper()).u[end]    
end
function ssa_get_u0_high(p_vals,jumps)
    disc_prob = DiscreteProblem([200,200,200], (0.,1000.), p_vals)
    jump_prob = JumpProblem(disc_prob, Direct(), jumps...)
    return solve(jump_prob, SSAStepper()).u[end]    
end;

### For Induction Simulations ###

function ssa_monte_p_steps(p_vals,tspan,n,p_steps;jumps=jumps_inducible,solver=SSAStepper(),eSolver=EnsembleThreads(),saveat=1.,find_u0=ssa_get_u0_base)
    u0s_cur = fill(find_u0(p_vals,jumps),n)
    p_cur = copy(p_vals)
    tstarts = vcat([tspan[1]],map(p_step -> p_step[2],p_steps)...)
    tends = vcat(map(p_step -> p_step[2],p_steps)...,[tspan[2]])
    sols = Vector{Any}(undef,length(tstarts))
    for i = 1:(length(p_steps)+1)
        disc_prob = DiscreteProblem(u0s_cur, (tstarts[i],tends[i]), p_cur)
        jump_prob = JumpProblem(disc_prob, Direct(),jumps...,save_positions=(false,false))
        ensemble_prob = EnsembleProblem(jump_prob,prob_func=(p,i,r)->JumpProblem(remake(p.prob,u0=u0s_cur[i]), Direct(),save_positions=(false,false), jumps...))
        sols[i] = solve(ensemble_prob,solver,eSolver,trajectories=n,saveat=saveat) 
        u0s_cur = map(sol -> sol.u[end], sols[i])
        (i!=(length(p_steps)+1)) && (setindex!(p_cur,p_steps[i][3],p_steps[i][1]))
    end
    return map(i -> (t=vcat(map(sol -> sol[i].t,sols)...), u=vcat(map(sol -> sol[i].u,sols)...)), 1:n)
end

### For continious Simulations
function cle_monte(p_vals,tspan,n,stress_steps=();solver=ImplicitEM(),eSolver=EnsembleThreads(),saveat=1.,adaptive=false,dt=0.001,find_u0=cle_get_u0_base)
    sde_prob = SDEProblem(f,g,find_u0(p_vals),tspan,p_vals,noise_rate_prototype=zeros(3,7))
    ensemble_prob = EnsembleProblem(sde_prob,prob_func=(p,i,r)->p)
    cbs = CallbackSet(positive_domain(),map(ss -> stress_step_cb(ss...),stress_steps)...)
    return solve(ensemble_prob,solver,eSolver,trajectories=n,callback=cbs,adaptive=adaptive,dt=dt,saveat=saveat,tstops=map(ss->ss[1],stress_steps))    
end
function cle_get_u0_base(p_vals)
    sde_prob = SDEProblem(f,g,[1.,1.,1.],(0.,500.),p_vals,noise_rate_prototype=zeros(3,7))
    return solve(sde_prob,ImplicitEM(),adaptive=false,dt=0.001,callback=positive_domain()).u[end]
end
function positive_domain()
    condition(u,t,integrator) = minimum(u) < 0
    affect!(integrator) = integrator.u .= integrator.uprev
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end
function stress_step_cb(step_time,step_amplitude)
    condition(u,t,integrator) = (t==step_time)
    affect!(integrator) = integrator.p[9] += step_amplitude
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end