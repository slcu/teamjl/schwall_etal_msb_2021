function MM_analyis_2021_05_21_v2(in_path)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mother Machine Data Analysis Script
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script analysis mother machine movies. The current path has to be set to path with
% the images to analyze e.g. C:\Sample_Data
% 
% 1. Pre-Analysis. 
%    This script automatically rotates all images such that the mother cell, the cell
%    that is at the dead end of the growth channel is the cell closest to 
%    the top edge of the image:
%   _______________
%    |0|  |0|  |0|
%    |0|  |0|  |0|
%    |0|  |0|  |0|
%    | |  |0|  |0|
%
% 2. Segmentation.
%    This function segments the images using an edge detection algorithm on
%    the RFP channel. The resulting image is undersegmented (all cells in a
%    channel might be recognized as one). To split cells a line along
%    the centre of the channels is used to determine where the cells end.
%    In the RFP channel at the split point the line profil has a minimum.
%
% 3. Tracking.
%    All cells in the channel are tracked. For the tracking to work it is 
%    critical that the mother cells is the cell closest to the top edge of the image.

tic;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 0. Set parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
date1='2016-06-14';
path=in_path;
cd(in_path);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1. Pre-Analysis 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Finding_channels_and_rotate_2021_05_25_v3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2. Segmentation 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cd([path,'\','subAuto']);
D=dir('*p-001.tif');

parfor i=1:length(D)
    p = initschnitz(D(i).name(1:11),'2016-06-14','bacillus','rootDir',cd,'imageDir',cd);
    p = segmoviefluor_mm_para_2021_05_25_v2(p);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3. Tracking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p = initschnitz('Bacillus-01','2016-06-14','bacillus','rootDir',cd,'imageDir',cd);
p.dataDir=[p.rootDir,'Data\'];
if ~exist(p.dataDir)
    mkdir(p.dataDir);
end
D=dir('Bacillus-01-p-*');
range=length(D);
p=track_all_Cells_2019_06_13_v6(p,range);

toc;
