function [axes_handle,figure_handle]=figure_special_pub_func_2019_10_30_v1(fig_para)
%Function to setup figure with the parameters in the structure fig_para

figure_handle=figure;%new figure
axes_handle=tight_subplot(1,3,0.07,fig_para.margin_h,[0.08,0.025]);
set(gcf, 'Units', 'centimeters', 'Position', fig_para.Position, 'PaperUnits', 'centimeters', 'PaperSize', [fig_para.Position(3), fig_para.Position(4)],'PaperType','a4');
%ha=tight_subplot(1,3,0.07,[0.15,0.025],[0.08,0.025]);

