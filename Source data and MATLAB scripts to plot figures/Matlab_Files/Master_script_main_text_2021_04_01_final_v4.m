function Master_script_main_text_2021_04_01_final_v4(in_path);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Master script to plot all figure in Main text
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%setting parameters;
p=initiate_SigV_figure_data_final_2021_03_25_v1(in_path);
out_addition='MSB-20-9832_';

% % 
%Figure 1 SigV Characterization
Figure1_2021_04_01_final_v4(p,out_addition);

% %Figure 2 Killing
Figure2_2021_04_01_final_v4(p,out_addition);

% %Figure 3 Circuit Characterization
Figure3_2021_04_15_final_v4(p,out_addition);

% 
% %Figure 4 Model
Figure4_2021_04_01_final_v4(p,out_addition);

%Figure 5 Genetic Pertubation
Figure5_2021_04_01_final_v4(p,out_addition);

%Figure 6 Memory 
Figure6_2021_04_01_final_v4(p,out_addition);






