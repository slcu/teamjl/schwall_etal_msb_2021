function Master_script_extended_view_2021_04_01_final_v1(in_path)
%Function to plot all EV figures

figure_name='MSB-20-9832_Figure_EV';
[p]=initiate_SigV_figure_data_final_2021_03_25_v1(in_path);

% %EV1
% %Single Cell traces P_sigV + lysozyme zoom in
figure_number='1';
EV1_2021_04_01_final_v1(p,figure_name,figure_number);


% %EV2
% %Turn on percentile
figure_number='2';
EV2_2021_04_01_final_v1(p,figure_name,figure_number);

% %EV3
% %OatA 20ug/ml
figure_number='3';
EV3_2021_04_01_final_v1(p,figure_name,figure_number);

% %EV4
% %plotting singel cell traces of model with lysozyme variation
figure_number='4';
EV4_2021_04_01_final_v1(p,figure_name,figure_number);


% %EV5
% %Rewiring the sigV network model and experiment fold change.
figure_number='5';
EV5_2021_04_01_final_v1(p,figure_name,figure_number);