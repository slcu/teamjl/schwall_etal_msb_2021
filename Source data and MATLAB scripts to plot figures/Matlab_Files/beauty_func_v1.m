function beauty_func_v1(fig_para)
set(gca,'Linewidth',fig_para.Linewidth,'FontSize',fig_para.FontSize,'Fontweight',fig_para.Fontweight,'FontName', fig_para.FontName);
set(gcf, 'Units', 'centimeters', 'Position', fig_para.Position, 'PaperUnits', 'centimeters', 'PaperSize', [fig_para.Position(3), fig_para.Position(4)],'PaperType', 'a4');
set(gca,'XColor','k','YColor','k','XTickLabelMode','auto','YTickLabelMode','auto');