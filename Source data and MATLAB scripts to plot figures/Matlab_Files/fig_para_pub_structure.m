function fig_para=fig_para_pub_structure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters for publication
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fig_para.Linewidth=2;
fig_para.FontSize=8;
fig_para.Fontweight='bold';
fig_para.FontName='Arial';
fig_para.Position=[10, 10, 18, 5.5];
fig_para.hline_width=1;
fig_para.PlotLinewidth=1;
fig_para.gap_h=0.1;
fig_para.margin_h=[0.15,0.025];
