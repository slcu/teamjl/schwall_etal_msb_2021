function ha=figure_special_func_pres(fig_para)
hh1=figure;%new figure
ha=tight_subplot_cs(2,2,0.08,fig_para.gap_h,fig_para.margin_h,[0.08,0.025]);
set(gcf, 'Units', 'centimeters', 'Position', fig_para.Position, 'PaperUnits', 'centimeters', 'PaperSize', [fig_para.Position(3), fig_para.Position(4)],'PaperType','A4');
