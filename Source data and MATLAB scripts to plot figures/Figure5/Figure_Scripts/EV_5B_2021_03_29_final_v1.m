function EV_5B_2021_03_29_final_v1(p,out_name,fig_para);
% Function to plot EV5B


%Loading Data
[fold_change]=loading_excel_file(p,out_name);




%setting x_val very small but larger 0 for plotting to work
fold_change.IPTG(1)=10^-100;
shadedErrorBar(fold_change.IPTG,fold_change.del,fold_change.del_std,'-r');
hold on;
shadedErrorBar(fold_change.IPTG,fold_change.WT,fold_change.WT_std,'-b');
set(gca,'XScale','log');
a=axis;
axis([1 10^3 a(3) a(4)]);
xlabel('IPTG in uM');
ylabel('Fold Change');
title('Experiment');
h=findobj(gca,'Type','line');
legend([h(1),h(4)],{'WT','No Feedback Loop'},'location','east','EdgeColor','k');
box on;
beauty_func_v1(fig_para);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [fold_change]=loading_excel_file(p,out_name);
in_path=p.Data_Excel;

if exist([in_path,out_name,'.xlsx'])
    %[~,sheet_names]=xlsfinfo([in_path,out_name]);
    sheet_names=sheetnames([in_path,out_name,'.xlsx']);
    in_data=xlsread([in_path,out_name],sheet_names{1});
    fold_change.IPTG=in_data(:,1);
    fold_change.WT=in_data(:,2);
    fold_change.WT_std=in_data(:,3);
    fold_change.del=in_data(:,4);
    fold_change.del_std=in_data(:,5);
else
    disp('No Excel files. Please generate first');
    return;
end



end

