function EV_5A_2021_03_29_final_v1(p,out_name,fig_para);

%Loading Data
[WT, Mutant]=loading_excel_file(p,out_name);



%Plotting Fold Change
shadedErrorBar(WT.concentration,WT.mean,WT.std,'-b');
hold on;
shadedErrorBar(Mutant.concentration,Mutant.mean,Mutant.std,'-r');
%set(gca,'XScale','log');
xlabel('Induction strength (au)');
ylabel('Fold Change');
title('Model');
h=findobj(gca,'Type','line');
legend([h(4),h(1)],{'WT','No Feedback Loop'},'location','east','EdgeColor','k');
box on;
beauty_func_v1(fig_para);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [WT,mutant]=loading_excel_file(p,out_name);
in_path=p.Data_Excel;

%Loading Histograms
if exist([in_path,out_name,'.xlsx']);
    %[~,sheet_names]=xlsfinfo([in_path,out_name]);
    sheet_names=sheetnames([in_path,out_name,'.xlsx']);
    in_data=xlsread([in_path,out_name],sheet_names)';
    %WT
    WT.concentration=in_data(1,:);
    WT.mean=in_data(2,:);
    WT.std=in_data(3,:);
    %Mutant
    mutant.concentration=in_data(1,:);
    mutant.mean=in_data(4,:);
    mutant.std=in_data(5,:);
else
    disp('No Excel files. Please generate first');
    return;
end

end