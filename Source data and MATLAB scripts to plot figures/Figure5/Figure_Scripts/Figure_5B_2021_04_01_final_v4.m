function Figure_5B_2021_04_01_final_v4(p,out_name,fig_para);
% Function to plot figure 5B

%Loading Data
[mutant_name_out,x_value,frac_use_m,frac_use_s]=loading_excel_file(p,out_name);


%plotting
bar(x_value,frac_use_m,'Facecolor','r');
hold on;
errorbar(x_value,frac_use_m,frac_use_s,'.k','CapSize',2);
beauty_func_v1(fig_para);
hline(frac_use_m(1));
ylabel('Fraction of Activated Cells (au)');
axis([0.5 4.5 0 1.05]);
title(['Model']);
mutant_name_out=cellfun(@(a) ['2x',a],mutant_name_out,'UniformOutput',false);
mutant_name_out{1}=mutant_name_out{1}(3:end);
set(gca,'Xticklabel',mutant_name_out);
set(gca,'XColor','k');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [mutant_name_out,x_value,frac_use_m,frac_use_s]=loading_excel_file(p,out_name);
in_path=p.Data_Excel;

if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    %converting number to string for condition
    [~,mutant_name_out_pre]=xlsread([in_path,out_name],sheet_names{1});
    mutant_name_out=mutant_name_out_pre(2:end);
    %Loading rest of data
    x_value=xlsread([in_path,out_name],sheet_names{2}); 
    frac_use_m=xlsread([in_path,out_name],sheet_names{3}); 
    frac_use_s=xlsread([in_path,out_name],sheet_names{4}); 
else
    disp('No Excel files. Please generate first');
    return;
end
end




