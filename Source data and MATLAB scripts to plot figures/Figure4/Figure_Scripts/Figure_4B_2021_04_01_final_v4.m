function Figure_4B_2021_04_01_final_v4(p,out_name,fig_para);
% Function to Figure 4B

%Loading Data
[condition,time_x,on_y]=loading_Model_turn_on_data_from_excel_file(p,out_name);


%plot SigV activation
plot(time_x,on_y,'Linewidth',1);

%Make figure nice
set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'},{'k','k','k'});
xlabel('Time (Minutes)');
ylabel('Cumulative Fraction (au)');
a=axis;
axis([0,a(2), 0, 1.05]);
box on;
beauty_func_v1(fig_para);
% Add line when stress was added
xline(500,'--k','Alpha',1);
legend(condition,'Location','southeast');

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [concentration_x,fold_change,fold_change_std]=loading_Model_turn_on_data_from_excel_file(p,out_name);
in_path=p.Data_Excel;

if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    %converting number to string for condition
    concentration_x_pre=readcell([in_path,out_name],'Sheet',sheet_names{1});
    concentration_x=cellfun(@num2str,concentration_x_pre(2,:), 'UniformOutput',false);
    %loading rest of data
    fold_change=xlsread([in_path,out_name],sheet_names{2});
    fold_change_std=xlsread([in_path,out_name],sheet_names{3}); 
else
    disp('No Excel files. Please generate first');
    return;
end
end

