function EV4_2021_04_01_final_v1(p,figure_name,figure_number);
% EV Figure 4. With increasing stress levels the heterogeneity 
% disappears in the mathematical model.  

%Setting parameters
out_name=[figure_name,figure_number];
current_path=p.EV4;

%Loading Data
[condition,time_x,traces]=loading_excel_file(current_path,out_name);


%setting up figure
fig_para=fig_para_pub_structure;
figure;
for i=1:length(time_x)
    %plotting
    subplot(2,3,i);
    plot(time_x{i},traces{i});
    %making figure nice
    xlabel('Time (au)');
    ylabel(['Number of ',char(963),'^{V} Molecules']);
    a=axis;
    axis([0 a(2) 0 350]);
    title(['Stress: ',condition{i}]);
    set(gca,'Linewidth',fig_para.Linewidth,'FontSize',fig_para.FontSize,'Fontweight',fig_para.Fontweight,'FontName', fig_para.FontName);
    set(gcf, 'Units', 'centimeters', 'PaperUnits', 'centimeters','PaperType', 'a4');
    set(gca,'XColor','k','YColor','k','XTickLabelMode','auto','YTickLabelMode','auto');
end

%saving data
print(gcf,'-painters','-depsc2',[current_path.Figure,out_name]);
exportgraphics(gcf,[current_path.Figure,out_name,'.pdf'],'ContentType','vector');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [concentration_x,time_x,traces]=loading_excel_file(p,out_name);
in_path=p.Data_Excel;
if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    ind=1;
    
    for i=1:length(sheet_names)
        f=strfind(sheet_names{i},'_');
        concentration_x{ind}=sheet_names{i}(f(end)+1:end);
        pre=xlsread([in_path,out_name],sheet_names{i});
        time_x{ind}=pre(:,1);
        traces{ind}=pre(:,2:end);
        ind=ind+1;
    end
else
    disp('No Excel files. Please generate first');
    return;
end
end

