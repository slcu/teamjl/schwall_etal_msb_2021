function Figure_4A_2021_04_01_final_v4(p,out_name,fig_para);
% Function to plot Figure 4A

%Loading Data
[out_data_x,out_data_y]=loading_excel_file(p,out_name);


%plottting

plot(out_data_x,out_data_y);

%make figure nice
xlabel('Time (Minutes)');
beauty_func_v1(fig_para);
ylabel(['Number of ',char(963),'^{V} Molecules'],'FontName','Arial');
axis([0,4000,0,350]);
xline(500,'--k','Alpha',1);
end

%%%%%%%%%%%%%%%%%%%%%%%
%Functions
%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [time_x,traces]=loading_excel_file(p,out_name);
%loading data from excel file

%setting parameters
in_path=p.Data_Excel;

%loading data
if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    pre=xlsread([in_path,out_name],sheet_names{1});
    %time
    time_x=pre(:,1);
    %traces
    traces=pre(:,2:end);
else
    disp('No Excel files. Please generate first');
    return;
end

end


