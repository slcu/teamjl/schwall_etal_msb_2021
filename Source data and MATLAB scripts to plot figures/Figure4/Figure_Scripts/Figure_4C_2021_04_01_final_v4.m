function Figure_4C_2021_04_01_final_v4(p,out_name, fig_para);
% Function to plot Figure 4C


%Loading Data
[concentration_x,fold_change,fold_change_std]=loading_excel_file(p,out_name);


%plot figure with errorbars
plot(concentration_x,fold_change,'k','Linewidth',1)
hold on; 
errorbar(concentration_x,fold_change,fold_change_std,'k.','Linewidth',1,'CapSize',2);
hold off

%Make figure nice
xlabel(['Lysozyme Concentration (au)']);
ylabel('Fold Change (au)');
beauty_func_v1(fig_para);
axis([0, 5, 0, 2*(fold_change(end)+fold_change_std(end))]);
set(gca,'Xtick',[0 1 2 3 4 5]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%1. Function to Load Excel Data
function [concentration_x,fold_change,fold_change_std]=loading_excel_file(p,out_name);
in_path=p.Data_Excel;

if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    %Loading concentrations
    concentration_x=xlsread([in_path,out_name],sheet_names{1});
    %Loading rest of data
    fold_change=xlsread([in_path,out_name],sheet_names{2});
    fold_change_std=xlsread([in_path,out_name],sheet_names{3}); 
else
    disp('No Excel files. Please generate first');
    return;
end
end
