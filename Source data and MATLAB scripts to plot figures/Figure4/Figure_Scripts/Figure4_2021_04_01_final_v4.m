function Figure4_2021_04_01_final_v4(p_in,out_addition)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Figure 4. A model of the simplified SigV circuit captures 
%experimental results.  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Setting Parameters
out_name_A=[out_addition,'Figure4A'];
out_name_B=[out_addition,'Figure4B'];
out_name_C=[out_addition,'Figure4C'];
out_name_fig4=[out_addition,'Figure4'];
p=p_in.Figure4;
p.bval=p_in.bval;

% %Setting Figure;
fig_para=fig_para_pub_structure;
[ha,~]=figure_special_pub_func_2019_10_30_v1(fig_para);

%Figure 4A Model Single Cell traces
axes(ha(1));
Figure_4A_2021_04_01_final_v4(p,out_name_A, fig_para);

%Figure 4B Model Turn on
axes(ha(2));
Figure_4B_2021_04_01_final_v4(p,out_name_B, fig_para);

%Figure 4C Model Fold Change
axes(ha(3));
Figure_4C_2021_04_01_final_v4(p,out_name_C, fig_para);


%saving part Circuit Overexpression
exportgraphics(gcf,[p.Figure,out_name_fig4,'.pdf'],'ContentType','vector');
print(gcf,'-painters','-depsc2',[p.Figure,out_name_fig4]);

