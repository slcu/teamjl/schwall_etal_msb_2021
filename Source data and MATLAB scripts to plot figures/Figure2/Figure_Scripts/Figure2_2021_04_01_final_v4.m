function Figure2_2021_04_01_final_v4(p_in,out_addition)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Figure 2. Rapid activation of SigV after a first stress application 
%increases survival after a second higher stress. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Setting Parameters
out_name_fig2=[out_addition,'Figure2'];
out_name_B=[out_addition,'Figure2B'];
out_name_C=[out_addition,'Figure2C'];
p=p_in.Figure2;

%Setting Figure;
fig_para=fig_para_pub_structure;
[ha,~]=figure_special_pub_func_2019_10_30_v1(fig_para);

% B) Lysis plot
axes(ha(2));
Figure_2B_2021_04_01_final_v4(p,out_name_B,fig_para);

% C) Cummulative MY
axes(ha(3));
Figure_2C_2021_04_01_final_v4(p,out_name_C,fig_para);

%Saving
exportgraphics(gcf,[p.Figure,out_name_fig2,'.pdf'],'ContentType','vector');
print(gcf,'-painters',[p.Figure,out_name_fig2],'-depsc2');