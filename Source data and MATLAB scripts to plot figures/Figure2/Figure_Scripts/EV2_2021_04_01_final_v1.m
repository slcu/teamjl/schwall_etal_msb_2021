function EV2_2021_04_01_final_v1(p_in,figure_name,figure_number)
% EV Figure 2. Cells with high PsigV-YFP before 
% stress activate SigV rapidly.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Setting parameters
out_name=[figure_name,figure_number];
p=p_in.EV2;
p.bval=p_in.bval;

%Loading Data
[time_on_top,MY_top,time_on_bottom,MY_bottom,time_on_all,names]=loading_excel_file(p,out_name);


%Setting up figure
fig_para=fig_para_pres_func_v1;
ha=figure_special_func_pres(fig_para);

for i=1:4
    %plotting
    axes(ha(i));
    plot(MY_bottom{i},time_on_bottom{i},'x','Linewidth',1.5);
    hold on; 
    plot(MY_top{i},time_on_top{i},'rx','Linewidth',1.5);
    axis([0 60 0 600 ]);
    a=axis;
    %Making figure nice
    text((a(2)-a(1))*0.6+a(1),(a(4)-a(3))*0.8+a(3),{['MTA = ',num2str(round(nanmean(time_on_all{i}))),char(177),num2str(round(nanstd(time_on_all{i}))),' min'],...
     ['MTA Red = ',num2str(round(nanmean(time_on_top{i}))),char(177),num2str(round(nanstd(time_on_top{i}))),' min'],...
     ['MTA Blue = ',num2str(round(nanmean(time_on_bottom{i}))),char(177),num2str(round(nanstd(time_on_bottom{i}))),' min'],...
     ['N = ',num2str(length(time_on_all{i}))],...
     ['N_{Red} = ',num2str(length(time_on_top{i}))],...
     ['N_{Blue} = ',num2str(length(time_on_bottom{i}))]},...
     'Fontsize',fig_para.FontSize-2);
    ylabel(['Time to activate ',char(963),'^V (min)']);
    xlabel(['P_{\it{sigV}}\rm\bf - YFP at stress application (au)']);
    title([names{i},char(181),'g/ml Lysozyme']);
    beauty_func_v1(fig_para);
end
%saving figure
print(gcf,'-painters',[p.Figure,out_name],'-depsc2');
exportgraphics(gcf,[p.Figure,out_name,'.pdf'],'ContentType','vector');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [time_on_top,MY_top,time_on_bottom,MY_bottom,time_on_all,names]=loading_excel_file(p,out_name);
in_path=p.Data_Excel;

if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    ind=1;
    %loading data
    for i=1:6:length(sheet_names)
        time_on_top{ind}=xlsread([in_path,out_name],sheet_names{0+i});
        MY_top{ind}=xlsread([in_path,out_name],sheet_names{1+i});
        time_on_bottom{ind}=xlsread([in_path,out_name],sheet_names{2+i});
        MY_bottom{ind}=xlsread([in_path,out_name],sheet_names{3+i});
        time_on_all{ind}=xlsread([in_path,out_name],sheet_names{4+i});
        MY_all{ind}=xlsread([in_path,out_name],sheet_names{5+i});
        f=strfind(sheet_names{i},' ');
        names{ind}=sheet_names{i}(1:f(1));
        ind=ind+1;
    end
else
    disp('No Excel files. Please generate first');
    return;
end
end
