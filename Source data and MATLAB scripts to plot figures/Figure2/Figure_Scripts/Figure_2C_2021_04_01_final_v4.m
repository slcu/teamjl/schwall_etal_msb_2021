 function Figure_2C_2021_04_01_final_v4(p,out_name,fig_para)
%Figure 2C plotting Cummulative Fractions 

%Getting data to plot
[data_m,data_s,data_p]=loading_excel_files(p,out_name);

    
%plotting Figure
plot(data_s(:,1),data_s(:,2:end),'-r');
hold on; 
plot(data_p(:,1),data_p(:,2:end),'-b');
shadedErrorBar_sigV(data_m(:,1),data_m(:,2),data_m(:,3),{'--r','Linewidth',1},1);
hold on; 
shadedErrorBar_sigV(data_m(:,1),data_m(:,4),data_m(:,5),{'--b','Linewidth',1},1);

%Making figure nice for shaded mean plot
h=findobj(gca,'Type','line');
legend([h(8),h(1),h(14),h(4)],{'Survivors','Mean Survivors','Perishers','MeanPerishers'},'FontSize',fig_para.FontSize-2,...
'Edgecolor','k','location','southeast');
set(gca,'YTickLabelMode', 'auto','XTickLabelMode', 'auto');
a=get(gca,'Position');
box on;
grid on;

xticks([0:0.2:1]);
yticks([0:0.2:1]);

xlabel('Normalized P_{sigV}-YFP(au)');
ylabel('Cumulative Fraction (au)');
axis([0 1 0 1]);
set(gca,'layer','top');
beauty_func_v1(fig_para);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [data_m,data_s,data_p]=loading_excel_files(p,out_name)
ind=1;
in_path=p.Data_Excel;
if exist([in_path,out_name,'.xlsx'])
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    for i=1:length(sheet_names)
        if i==1
            %loading mean data
            data_m=xlsread([in_path,out_name],sheet_names{i});
        elseif i==2
            %loading data of survivors
            data_s=xlsread([in_path,out_name],sheet_names{i});
        else
            %loading data of perishers
            data_p=xlsread([in_path,out_name],sheet_names{i}); 
        end
    end
else
    disp('No Excel files. Please generate first');
    return;
end
end



