function Figure3_2021_04_15_final_v4(p_in,out_addition)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Figure3 Circuit Characterization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%Setting Parameters
out_name_B=[out_addition,'Figure3B'];
out_name_C=[out_addition,'Figure3C'];
out_name_D=[out_addition,'Figure3D'];
out_name_E=[out_addition,'Figure3E'];
out_name_fig3=[out_addition,'Figure3'];
p=p_in.Figure3;
p.bval=p_in.bval;


% %Setting Figure;
fig_para=fig_para_pub_structure;
[ha,~]=figure_special_pub_func_2019_10_30_v1(fig_para);

% %Figure 3B WT no stress and WT with stress
axes(ha(2));
Figure_3B_2021_04_09_final_v4(p,out_name_B,fig_para);


%Figure 3C DelsigV no stress and DelsigV with stress
axes(ha(3));
Figure_3C_2021_04_09_final_v4(p,out_name_C,fig_para);


% % % %saving partI RNA seq
print(gcf,'-painters','-depsc2',[p.Figure,out_name_fig3,'_part1']);
exportgraphics(gcf,[p.Figure,out_name_fig3,'_part1.pdf'],'ContentType','vector');


%Setting Figure;
fig_para=fig_para_pub_structure;
[ha,~]=figure_special_pub_func_2019_10_30_v1(fig_para);


%Figure 3D Circuit Overexpression
axes(ha(1));
Figure_3D_2021_04_15_final_v4(p,out_name_D,fig_para);


%Figure 3E OatA Over expression
axes(ha(2));
Figure_3E_2021_04_01_final_v4(p,out_name_E,fig_para);


%saving partII Circuit Overexpression
print(gcf,'-painters','-depsc2',[p.Figure,out_name_fig3,'_part2']);
exportgraphics(gcf,[p.Figure,out_name_fig3,'_part2.pdf'],'ContentType','vector');




