function EV3_2021_04_01_final_v1(p_in,figure_name,figure_number)
%EV Figure 3 Overexpressing oatA shuts off SigV activation. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Setting Parameters
p=p_in.EV3;
p.bval=p_in.bval;
out_name=[figure_name,figure_number];
x_label_name={['WT+0',char(181),'g/ml'],['WT+1',char(181),'g/ml'],['oatA+1',char(181),'g/ml'], ['oatA+20',char(181),'g/ml']};


%loading data
[box_data]=loading_excel_files(p,out_name);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Making oatA subfigure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %Setting Figure;
fig_para=fig_para_pub_structure;
[ha,~]=figure_special_pub_func_2019_10_30_v1(fig_para);
axes(ha(1));
h=boxplot(box_data,'Symbol','');
axis([0 13 0 1000]);
beauty_func_v1(fig_para);
set(gca,'XTick',[2 5 8 11],'XTickLabel',x_label_name);
ylabel('P_{\it{sigV}}\rm\bf - YFP (au)')
ax=gca;
ax.XAxis.FontSize = 6;
print(gcf,'-painters',[p.Figure,out_name],'-depsc2');
exportgraphics(gcf,[p.Figure,out_name,'.pdf'],'ContentType','vector');
end

%1. Function to Load Excel Data
function [box_data]=loading_excel_files(p,out_name);
in_path=p.Data_Excel;
if exist([in_path,out_name,'.xlsx']);
    %loading data
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    box_data=xlsread([in_path,out_name],sheet_names{3});
else
    disp('No Excel files. Please generate first');
    return;
end
end

