function Figure_3C_2021_04_09_final_v4(p,out_name,fig_para);
% Plotting RNA-Seq data for del sigV

%Loading Data
[t_line_x,t_line_y,delsigV_no_stress,delsigV_stress]=loading_excel_file(p,out_name);


%plotting RNA-Seq data
loglog(delsigV_no_stress,delsigV_stress,'kx');
hold on;
plot(t_line_x,t_line_y,'--','color',[0.5 0.5 0.5]);

%shading
x=1:1:10^6;
curve1=[1:1:10^6]*5;
curve2=[1:1:10^6]*0.2;
pg=polyshape([1 1 10^6 10^6], [0.2 5 5*10^6 0.2*10^6]);
h=plot(pg);
set(h,'FaceColor',[0.92,0.92,0.92],'FaceAlpha',0.5,'EdgeColor',[0.80,0.80,0.80]);
%plot(x,[curve1;curve2],'-k');
% x2 = [x, fliplr(x)];
% inBetween = [curve1, fliplr(curve2)];
% h_fill=fill(x2, inBetween, [0.92,0.92,0.92]);
% set(h_fill,'FaceAlpha',0.5,'EdgeColor',[0.80,0.80,0.80]);

%Making Figure nice
xlabel([char(916),'sigV No Stress']);
ylabel([char(916),'sigV + 1 ',char(181),'g/ml Lysozyme']);

set(gca,'XTick',[1, 10,100,1000,10000,100000,1000000]);
axis([0 10^6 0 10^6]);
h=findobj(gca,'Type','line');
beauty_func_v1(fig_para);
set(gca,'Linewidth',1);
[hh,icons,~,~] = legend([h(1),h(2)],{'No Change','Responsive Genes'},'Location','southeast');
set(hh,'FontSize',4,'EdgeColor','k');
% Making Legend Nice
p1 = icons(1).Position;
icons(1).Position = [0.3 p1(2) 0];
icons(4).XData = [0.05 0.25];
p2 = icons(2).Position;
icons(2).Position = [0.3 p2(2) 0];
end






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%1. Function to Load Excel Data
function [t_line_x,t_line_y,delsigV_no_stress,delsigV_stress]=loading_excel_file(p,out_name)
in_path=p.Data_Excel;

if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    for i=1:length(sheet_names);
        if i==2
            delsigV_no_stress=xlsread([in_path,out_name],sheet_names{i});
        elseif i==3
            delsigV_stress=xlsread([in_path,out_name],sheet_names{i});
        elseif i==4
            t_line_x=xlsread([in_path,out_name],sheet_names{i}); 
        elseif i==5
            t_line_y=xlsread([in_path,out_name],sheet_names{i});
        end
    end
else
    disp('No Excel files. Please generate first');
    return;
end
end

