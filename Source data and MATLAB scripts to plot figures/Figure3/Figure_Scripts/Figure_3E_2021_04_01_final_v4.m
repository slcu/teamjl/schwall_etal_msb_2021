function Figure_3E_2021_04_01_final_v4(p,out_name,fig_para)
% Plotting Figure 3E

%Loading Data
[x_val,frac_m,frac_s,t_name]=loading_excel_file(p,out_name);


%Plotting Figure
bar(x_val,frac_m,'FaceColor','r');
hold on;
errorbar(x_val,frac_m,frac_s,'.k');
%Making Figure nice
beauty_func_v1(fig_para);
axis([0.25 4.75 -0.05 1]);
set(gca,'XTickLabel',t_name,'XTickLabelRotation',45);
ylabel('Fraction of Activated Cells (au)');

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [x_val,frac_use_m,frac_use_s,t_name]=loading_excel_file(p,out_name);
in_path=p.Data_Excel;

if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    %converting number to string for condition
    [~,mutant_name_out_pre]=xlsread([in_path,out_name],sheet_names{1});
    t_name=mutant_name_out_pre(2:end);
    x_val=xlsread([in_path,out_name],sheet_names{2}); 
    frac_use_m=xlsread([in_path,out_name],sheet_names{3}); 
    frac_use_s=xlsread([in_path,out_name],sheet_names{4}); 
else
    disp('No Excel files. Please generate first');
    return;
end
end

