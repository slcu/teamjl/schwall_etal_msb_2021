Extract the folder and run the following matlab script to plot the data: 
plot_sigV_paper_figures_2021_04_01_final_v2 

The scripts run well on MATLAB 2020a and later versions need the 
following MATLAB package to run:
'Statistics and Machine Learning Toolbox'

