function Figure_1E_2021_04_01_final_v4(p,out_name,fig_para)
% Function to plot Figure 1E

%loading Data
[time_all,MY_all]=loading_excel_data(p,out_name);



%Plotting Main Figure data
plot(time_all,MY_all);

%Making figure nice
xlabel('Time (Minutes)');
ylabel('P_{\it{sigV}}\rm\bf - YFP (au)');
axis([0 800 0 1500]);
beauty_func_v1(fig_para);
xline(240,'--k','Linewidth',1,'Alpha',1);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%1. Function to Load Excel Data
function [x_val,y_val]=loading_excel_data(p,out_name)
out_path=p.Data_Excel;
if exist([out_path,out_name,'.xlsx'])
    [~,sheet_names]=xlsfinfo([out_path,out_name]);
    x_val=xlsread([out_path,out_name],sheet_names{1});
    y_val=xlsread([out_path,out_name],sheet_names{2});
else
    disp('No Excel files. Please generate first');
    return;
end
end


