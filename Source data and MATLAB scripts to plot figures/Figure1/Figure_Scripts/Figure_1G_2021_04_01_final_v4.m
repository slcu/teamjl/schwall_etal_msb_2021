function Figure_1G_2021_04_01_final_v4(p_use,out_name,fig_para)
% Function to plot Figure 1G


%Setting Parameters
xpos=[-0.1,0.1,0.4,0.6,0.9,1.1,1.9,2.1,3.9,4.1];

%Loading Data
[x_val,y_val]=loading_excel_file(p_use,out_name);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ploting Fold Change
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the figure has to build up
% 1. plot lines
% 2. plot filling of box plot
% 3. plot actual box plot

%%%%%%%%%%%%%%%%%%%%%%%%%%%part1 plotting line%%%%%%%%%%%%%%%%%%%%%%%%%%%
h1=plot(x_val{1},y_val{1},'k');
hold on;

%%%%%%%%%%%%%%%%%%%%%%%%%%part2 plotting filling%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the boxstyle filling makes filles the box plot
h2=boxplot(y_val{2},x_val{2}, 'positions', xpos,'colors','w','Width',0.05,'Symbol','','BoxStyle' ,'filled');
[G,~] = findgroups(x_val{2});
%set width of box. its a pain 
a = get(get(gca,'children'),'children');   % Get the handles of all the objects
t = get(a{1},'tag');   % List the names of all the objects 
idx=strcmpi(t,'box');  % Find Box objects
boxes=a{1}(idx);          % Get the children you need
set(boxes,'linewidth',6); % Set width

%%%%%%%%%%%%%%%%%%%%%%part 3 plotting actual boxes%%%%%%%%%%%%%%%%%%%%%%%%%%
hold on;
h=boxplot( y_val{2},x_val{2},'positions', xpos,'colors','k','Width',0.1,'Symbol','');




%%%%%%%%%%%% Making figure nice %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%setting line width
set(h,{'linew'},{1});
% making wiskers solid lines
ho=findobj(gcf,'LineStyle','--');
set(ho(3:end),'LineStyle','-');

% setting ticks and minor ticks
set(gca,'Xtick',[0 1 2 3 4],'Xticklabel',{'0','1','2','3','4'},'XMinorTick','on')
a=gca;
a.XAxis.MinorTickValues = 0.5:1:4;
a=axis;
axis([-0.3,4.3,a(3) 1000]);
%setting median color red
lines = findobj(gcf, 'type', 'line', 'Tag', 'Median');
set(lines, 'Color', 'r');

% making guidance line dashed. this has to be done last as the previous
% set(findobj(gcf,'LineStyle','--'),'LineStyle','-'); concerts all dashed
% lines into lines.
set(h1,'LineStyle','--','Linewidth',1.2);

% % Make figure nice
a=get(gca,'Position');
xlabel(['Lysozyme Concentration (',char(181),'g/ml)'],'FontSize',fig_para.FontSize,'Fontweight',fig_para.Fontweight,'FontName', fig_para.FontName);
ylabel('Fold Change (au)','FontSize',fig_para.FontSize,'Fontweight',fig_para.Fontweight,'FontName', fig_para.FontName);
set(gca,'Position',a);
beauty_func_v1(fig_para);
set(gca, 'Layer', 'Top');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [x_val,y_val]=loading_excel_file(p,out_name)
in_path= p.Data_Excel;
ind=1;
if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    for i=1:length(sheet_names);
        if mod(i,2)~=0
            %case of x values
            x_val{ind}=xlsread([in_path,out_name],sheet_names{i});
        else
            %case of y values
            y_val{ind}=xlsread([in_path,out_name],sheet_names{i});
            ind=ind+1;
        end
    end
else
    disp('No Excel files. Please generate first');
    return;
end
end


