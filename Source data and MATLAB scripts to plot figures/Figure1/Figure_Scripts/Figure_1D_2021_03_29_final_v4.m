function Figure_1D_2021_03_29_final_v4(p_use,out_name,fig_para)
% Function to plot Figure 1D single cell traces


%Loading Data
[time,MY]=loading_excel_file(p_use,out_name);


%Plotting Main Figure data
plot(time,MY);

%Making figure nice
xlabel('Time (Minutes)');
ylabel('P_{\it{sigV}}\rm\bf - YFP (au)');

%setting Axis
axis([0 800 0 7000]);

beauty_func_v1(fig_para);
xline(240,'--k','Linewidth',1,'Alpha',1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [x_val,y_val]=loading_excel_file(p,out_name)
if exist([p.Data_Excel,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([p.Data_Excel,out_name]);
    x_val=xlsread([p.Data_Excel,out_name],sheet_names{1});
    y_val=xlsread([p.Data_Excel,out_name],sheet_names{2});
else
    disp('No Excel files. Please generate first');
    return;
end
end


