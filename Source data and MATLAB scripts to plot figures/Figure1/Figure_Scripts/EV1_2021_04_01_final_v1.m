function EV1_2021_04_01_final_v1(p_in,figure_name,figure_number)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%EV Figure 1. With increasing stress levels the heterogeneity 
%in SigV activation times are reduced.  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
out_name=[figure_name,figure_number];
p=p_in.EV1;
p.bval=p_in.bval;


%Loading Data
[t_name,x_val,y_val]=loading_excel_file(p,out_name);


%Setting up figure
figure;
fig_para=fig_para_pres_func_v1;
fig_para.gap_h=0.1;
fig_para.margin_h=[0.025,0.025];
set(gcf, 'Units', 'centimeters','PaperUnits', 'centimeters', 'PaperPosition',[0 0 19 25],'PaperSize', [19, 25], 'PaperType','A4',...
    'Position',[15,3,19,25]);
ha=tight_subplot_cs(4,2,0.08,0.05,fig_para.margin_h,[0.08,0.025]);

max_y=[800,1800,2000,2500];
ind=1;
for i=1:length(y_val)
    %plotting
    axes(ha(i));
    plot(x_val{i},y_val{i});
    axis([0 800 0 max_y(ind)]);
	%Adding labels
    xlabel('Time (Minutes)');
    ylabel('P_{\it{sigV}}\rm\bf - YFP (au)')
    xline(240,'--k','Linewidth',1,'Alpha',1);
    n=size(y_val{i},2);
    a=axis;
    text(a(2)*0.1,a(4)*0.7,['N = ',num2str(n)]);
    %setting correct title
    if mod(i,2)==1
        title([t_name{i},' ',char(181),'g/ml Lysozyme 1. Repeat']);
    else
        title([t_name{i},' ',char(181),'g/ml Lysozyme 2. Repeat']);
        ind=ind+1;
    end
    box on;
    %Making figure nice
    set(gca,'Linewidth',fig_para.Linewidth,'FontSize',fig_para.FontSize,'Fontweight',fig_para.Fontweight,'FontName', fig_para.FontName);
    set(gca,'XColor','k','YColor','k','XTickLabelMode','auto','YTickLabelMode','auto');
end

print(gcf,'-painters',[p.Figure,out_name],'-depsc2');
exportgraphics(gcf,[p.Figure,out_name,'.pdf'],'ContentType','vector');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions for script
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%1. Function to Load Excel Data
function [t_name,x_val,y_val]=loading_excel_file(p,out_name)
ind=1;
if exist([p.Data_Excel,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([p.Data_Excel,out_name]);
    for i=1:length(sheet_names);
        if mod(i,2)~=0
            %loading time
            x_val{ind}=xlsread([p.Data_Excel,out_name],sheet_names{i});
            f=strfind(sheet_names{i},' ');
            t_name{ind}=sheet_names{i}(f(1)+1:f(2)-1);
        else
            %Loading traces
            y_val{ind}=xlsread([p.Data_Excel,out_name],sheet_names{i});
            ind=ind+1;
        end
    end
    
else
    disp('No Excel files. Please generate first');
    return;
end

end



