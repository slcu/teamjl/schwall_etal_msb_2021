function Figure_6F_2021_04_01_final_v4(p,out_name,fig_para)
% Function to Figure 6F

%Loading Data
[holiday_length,time_x,mat_aligned]=loading_excel_file(p,out_name);


%plotting
plot(time_x,mat_aligned,'Linewidth',1.5);
axis([-200 600 0 1.1]);
%making figure nice
xlabel('Time (Minutes)');
ylabel('Cumulative Fraction (au)');
beauty_func_v1(fig_para);
legend({holiday_length{1},holiday_length{2},holiday_length{3}},'location','southeast','FontSize',5,'EdgeColor','k');

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [holiday_length,time_x_out,mat_aligned_out]=loading_excel_file(p,out_name);
in_path=p.Data_Excel;

if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    %converting number to string for condition
    [~,holiday_length_pre]=xlsread([in_path,out_name],sheet_names{1});
    holiday_length=holiday_length_pre(2,:);
    %loading data
    time_x_out=xlsread([in_path,out_name],sheet_names{2});
    mat_aligned_out=xlsread([in_path,out_name],sheet_names{3}); 
else
    disp('No Excel files. Please generate first');
    return;
end
end

